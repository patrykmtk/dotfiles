set nocompatible
filetype off

" junegunn/vim-plug
call plug#begin()

Plug 'scrooloose/nerdtree'	" tree explorer
Plug 'tpope/vim-fugitive'	" Git wrapper
"Plug 'vim-scripts/a.vim'	" Quick switching between source & header
Plug 'Raimondi/delimitMate'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'airblade/vim-gitgutter'
Plug 'brookhong/cscope.vim'
Plug 'vim-scripts/taglist.vim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'majutsushi/tagbar'
"causes lags Plug 'Yggdroot/indentLine'

call plug#end()

filetype plugin indent on

" To ignore plugin indent changes, instead use:
filetype plugin on

set number
set linebreak
set showbreak=+++
set textwidth=100
set showmatch
set visualbell
set relativenumber

set smartcase
set ignorecase                                  " Ignore case when searching
set incsearch                                   " Search as you type

set wildmenu                                    " Show autocomplete menus
set autoindent
set expandtab
set shiftwidth=4
set smartindent
set smarttab
set softtabstop=4
set foldmethod=indent
set showcmd                                     " Display incomplete commands
set ruler

set undolevels=1000
set backspace=indent,eol,start
set foldlevel=99

inoremap <c-space> <c-n>

nmap <F8> :TagbarToggle<CR>

map <silent> <F9> :NERDTreeToggle<CR>
map! <silent> <F9> :NERDTreeToggle<CR>
let g:ctrlp_cmd = 'CtrlPTag'

" For airline
set laststatus=2
set ttyfast
set updatetime=250
